package tester;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import javax.sql.*;
import java.util.*;

/*
 * Database tester
 * 
 * Simple command-line testing tool that hits the repository a configurable number of times
 * 
 * Areas to develop:
 * - Make it so that it tests different drivers
 * - Have it do a multi-threaded test
 * - Have it go through a datasource defined in Tomcat
 */

public class DBTester {

	private String hostname;
	private String portNumber;
	private String dbName;
	private String dbType;
	private String dbUser;
	private String dbPassword;
	private String delimiter = ":";
	private ArrayList<String> queries = null;
	private String showJdbcUrl = "false";
	private String printQueries = "false";
	
	/**
	 * @param db.properties filename
	 * @param queries filename
	 */
	public static void main(String[] args) {
		
		if (args.length < 2) {
			log("Usage: java -jar dbtester.jar db.properties queries.txt");
			System.exit(1);
		}
		
		String fFileName = args[0];
		String queriesFile = args[1];
		
		DBTester st = new DBTester();
		
		try {
			st.readFileArgs(fFileName, false);
			st.readFileArgs(queriesFile, true);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

		if (st.printQueries.equals("true")) {
			st.printQueries();
		}
		
		log("Query Executed,No. of Times Executed,Elapsed Time (only query execution),Total Elapsed Time (including connection time)");
		st.runLoadTestSequential(50);
		st.runLoadTestSequential(500);
		st.runLoadTestSequential(2000);
		
	}
	
	public DBTester() {
		queries = new ArrayList<String>();
	}
	
	private  String buildJDBCURLString(String hostName, String portNumber, String databaseName, String databaseType, String delimiter) {
		
        String jdbcURL = null;
		if (databaseType.toLowerCase().equals("mysql")) {
			jdbcURL = "jdbc:mysql://" + hostName + ":" + portNumber + "/" + databaseName + "?useUnicode=true&amp;characterEncoding=UTF-8&amp;autoReconnect=true&amp;autoReconnectForPools=true";
		} else if (databaseType.toLowerCase().equals("oracle")) {
			jdbcURL = "jdbc:oracle:thin:@" + hostName + ":" + portNumber + delimiter;
			if (!databaseName.toLowerCase().equals("none")) {
				jdbcURL = jdbcURL + databaseName;
			}
		} else { 
			jdbcURL = "jdbc:postgresql://" + hostName + ":" + portNumber + "/" + databaseName;
		}

		if (showJdbcUrl.equals("true")) {
			log("JDBC URL = " + jdbcURL);
		}
		return jdbcURL;
	}
	
	private void readFileArgs(String fFileName, boolean forQueries) throws IOException {

		Scanner scanner = new Scanner(new FileInputStream(fFileName));
		try {
			while (scanner.hasNextLine()) {
				String nextLine = scanner.nextLine();
				
				if (forQueries) {
					queries.add(nextLine);
				} else {
					String [] values = nextLine.split("=");
					assignValue(values);	
				}				
			}
		}
		finally {
			scanner.close();
		}
	}
	  
	private static void log(String aMessage){
		System.out.println(aMessage);
	}
	
	private void runLoadTestSequential(int timesToExecute) {
		String dbClass = "";
		String jdbcURL = buildJDBCURLString(hostname, portNumber, dbName, dbType, delimiter);
		dbClass = getClassName(dbType);
		Connection con = null;
		
		try {
			int queryIteration = 1;
			for (String query : queries) {
				Class.forName(dbClass);
				long startConnection = System.currentTimeMillis();
				con = DriverManager.getConnection (jdbcURL, dbUser, dbPassword);
				long startStatement = System.currentTimeMillis();
				
				for (int i = 0; i < timesToExecute; i++) {
					Statement stmt = con.createStatement();
					ResultSet rs = stmt.executeQuery(query);	
					stmt.close();
				}
				
				long endPoint = System.currentTimeMillis();
				log(queryIteration + "," + timesToExecute + "," + (endPoint - startStatement) + "," + (endPoint - startConnection));
				queryIteration++;
			}
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (con != null && !con.isClosed()) {
					con.close();
				}
			} catch (SQLException sqlex) {
				sqlex.printStackTrace();
			}
		}
	}
	
	private void assignValue(String [] values) {
		if (values[0].equals("hostname")) {
			hostname = values[1];
		} else if (values[0].equals("portNumber")) {
			portNumber = values[1];
		} else if (values[0].equals("dbName")) {
			dbName = values[1];
		} else if (values[0].equals("dbType")) {
			dbType = values[1];
		} else if (values[0].equals("dbUsername")) {
			dbUser = values[1];
		} else if (values[0].equals("dbPassword")) {
			dbPassword = values[1];
		} else if (values[0].equals("delimiter")) {
			delimiter = values[1];
		} else if (values[0].equals("showJdbcUrl")) {
			showJdbcUrl = values[1];
		} else if (values[0].equals("printQueries")) {
			printQueries = values[1];
		}
	}
	
	private static String getClassName(String dbType) {
		String dbClass = "";
		if (dbType.toLowerCase().equals("mysql")) {
			dbClass = "com.mysql.jdbc.Driver";
		} else if (dbType.toLowerCase().equals("oracle")) {
			dbClass = "oracle.jdbc.OracleDriver";
		} else if (dbType.toLowerCase().equals("postgres")) {
			dbClass = "org.postgresql.Driver";
		} else {
			log("Database type not recognized - choose oracle, postgres or mysql");
			System.exit(1);
		}
		
		return dbClass;

	}
	
	public void printQueries() {
		int num = 1;
		for (String q : queries) {
			log("Query " + num + ":" + q);
			num++;
		}
	}
}
